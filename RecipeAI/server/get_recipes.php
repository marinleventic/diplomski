<?php
header("Access-Control-Allow-Origin: *");
header("Content-type:application/json");

$REQUESTED_NUMBER = 70;

class Recipe
{
    public $ingredient_list;
    public $instructions;
    public $nutrients;
    public $total_steps;
    public $recipe;
    public $category;
    public $nr;
    public $lim3;
    public $nrf;
    public $nr2;
    public $lim32;
    public $nrf2;
    public $url;
    public $score;
    public $prep_time;
    public $cook_time;
    public $ready_time;
    public $calories;
    public $servings;
    public $carb_per;
    public $fat_per;
    public $protein_per;
    public $total_per;
    public $cumulative_score_1;
    public $cumulative_score_2;
    public $ingredients_in_recipe;
    public $ingredients_in_recipe_num;
    public $macronutrient_balance;
    public $reviews;
    public $carb_score;
    public $fat_score;
    public $protein_score;
    public $macronutrient_score;
}

function regular_expression($ingredient, $ingredient_string)
{
    //  '/\b({0})(es|s)?\b/'
    $ingredient = preg_replace("/\b(es|s)?\b/","", $ingredient);
    return stripos($ingredient_string, $ingredient);
}

function find_ingredients($ingredients, $ingredient_list)
{
    $present_ingredients = array();
    $first = true;

    for($i = 0; $i < count($ingredient_list); $i++)
    {
        if ($first)
        {
            $ingredient_string = $ingredient_list[$i];
            $first = false;
        }
        else
        {
            $ingredient_string .= " ";
            $ingredient_string .= $ingredient_list[$i];
        }
    }
    foreach ($ingredients as $ingredient)
    {
        if(regular_expression($ingredient, $ingredient_string) !== false)
        {
            array_push($present_ingredients, $ingredient);
        }
    }
    return $present_ingredients;
}

function calculate_macronutrient_balance($fat_per, $carb_per, $protein_per)
{
    $balance = 0;
    if($fat_per >= 25 && $fat_per <= 35)
    {
        $balance += 1;
    }
    if($carb_per >= 45 && $carb_per <= 65)
    {
        $balance += 1;
    }
    if($protein_per >= 10 && $protein_per <= 35)
    {
        $balance += 1;
    }
    return $balance;
}

function get_request_data($ingredients, $category, $requested_number, $from_number, $to_number)
{
    require('/admin/db_login.php');
    $dictRecipes = array();
    $valDividedRecipes = array();

    $ingr_list_length = count($ingredients);

    //Mysql can use only 61 tables in join
    if ($ingr_list_length >= 60)
    {
        $ingr_list_length = 60;
    }
    try 
    {
        $recipe_ids = array();

        foreach (range(0, $ingr_list_length) as $i)
        {
            try
            {
                if ($category === "All")
                {
                    $result = (mysqli_query($connection, "SELECT recipe_id FROM recipe_ingredient 
                    INNER JOIN ingredient ON recipe_ingredient.ingredient_id = ingredient.id
                    INNER JOIN recipe ON recipe_ingredient.recipe_id = recipe.id
                    WHERE ingredient.ingredient = '$ingredients[$i]' ORDER BY recipe.score DESC, recipe.nrf DESC")) or die("Could not execute query");
                    if($result !== false)
                    {
                        while ($row = $result->fetch_array(MYSQLI_ASSOC))
                        {
                            array_push($recipe_ids, $row['recipe_id']);
                        }
                    }
                }
                else
                {
                    $result = (mysqli_query($connection, "SELECT recipe_id FROM recipe_ingredient 
                    INNER JOIN recipe ON recipe_ingredient.recipe_id = recipe.id 
                    INNER JOIN ingredient ON recipe_ingredient.ingredient_id = ingredient.id 
                    INNER JOIN category ON recipe.category_id = category.id 
                    WHERE ingredient.ingredient = '$ingredients[$i]' AND category.category = '$category' ORDER BY recipe.score DESC, recipe.nrf DESC")) or die("Could not execute query");
                    if($result !== false)
                    {
                        while ($row = $result->fetch_array(MYSQLI_ASSOC))
                        {
                            array_push($recipe_ids, $row['recipe_id']);
                        }
                    }
                }
            }
            catch (Exception $e) 
            {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                echo "Ingredient does not exist";
            }
        }

        //dict for asserting number of times required ingredients show in specific recipe
        $dictRecipes = array_fill_keys($recipe_ids, 0);
        foreach($recipe_ids as $recipe_id)
        {
            try
            {
                $dictRecipes[$recipe_id] += 1;
            }
            catch (Exception $e)
            {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        $dict_len = count($dictRecipes);

        if($requested_number != 0)
        {
            $from_number = 0;
            $to_number = $requested_number;
            if($to_number > $dict_len)
            {
                $to_number = $dict_len;
            }
        }
        else
        {
            try
            {
                if($from_number > $dict_len)
                {
                    $from_number = $dict_len - 10;
                }

                if($to_number > $dict_len)
                {
                    $to_number = $dict_len;
                }
            }
            catch (Exception $e)
            {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                echo "Error with requested from-to numbers";
            }
        }
        $recipes = array();
        $counter = $ingr_list_length;
        while ((count($recipes) < $to_number) && ($counter > 0))
        {
            //divide recipes by number of matching ingredients
            $valDividedRecipes[$counter] = array_keys($dictRecipes, $counter);

            foreach($valDividedRecipes[$counter] as $rcp_id)
            {
                if(count($recipes) >= $to_number)
                {
                    break;
                }

                //fetching data for each recipe
                $result = mysqli_query($connection, "SELECT recipe.recipe, category.category, recipe.nr, recipe.lim3, recipe.nrf, recipe.nr2, recipe.lim32, recipe.nrf2, recipe.url, recipe.score, recipe.prep_time,
                recipe.cook_time, recipe.ready_time, recipe.calories, recipe.servings, recipe.carb_per, recipe.fat_per, recipe.protein_per, recipe.total_per, recipe.cumulative_score_1, recipe.cumulative_score_2, recipe.reviews
                FROM recipe INNER JOIN category ON recipe.category_id = category.id WHERE recipe.id = $rcp_id") or die("Could not execute query");

                if($result !== false)
                {
                    if($row = $result->fetch_array(MYSQLI_ASSOC))
                    {
                        if(!array_key_exists($rcp_id, $recipes))
                        {
                            $macronutrient_balance = calculate_macronutrient_balance($row['fat_per'], $row['carb_per'], $row['protein_per']);

                            //calculate carb_score, protein_score, fat_score and macronutrient_score
                            if ($row['carb_per'] < 45) { $carb_score = $row['carb_per'] / 45; }
                            else if  (($row['carb_per'] >= 45) && ($row['carb_per'] <= 65)) { $carb_score = 1; }
                            else if  ($row['carb_per'] >= 65) { $carb_score = -(65 / $row['carb_per']); }

                            if ($row['fat_per'] < 25) { $fat_score = $row['fat_per'] / 25; }
                            else if  (($row['fat_per'] >= 25) && ($row['fat_per'] <= 35)) { $fat_score = 1; }
                            else if  ($row['fat_per'] >= 35) { $fat_score = -(35 / $row['fat_per']); }

                            if ($row['protein_per'] < 10) { $protein_score = $row['protein_per'] / 10; }
                            else if  (($row['protein_per'] >= 10) && ($row['protein_per'] <= 35)) { $protein_score = 1; }
                            else if  ($row['protein_per'] >= 35) { $protein_score = -(35 / $row['protein_per']); }

                            $macronutrient_score = ($carb_score + $fat_score + $protein_score) / 3;
                            
                            $recipes[$rcp_id] = new Recipe();
                            $recipes[$rcp_id]->recipe = $row['recipe'];
                            $recipes[$rcp_id]->category = $row['category'];
                            $recipes[$rcp_id]->nr = $row['nr'];
                            $recipes[$rcp_id]->lim3 = $row['lim3'];
                            $recipes[$rcp_id]->nrf = $row['nrf'];
                            $recipes[$rcp_id]->nr2 = $row['nr2'];
                            $recipes[$rcp_id]->lim32 = $row['lim32'];
                            $recipes[$rcp_id]->nrf2 = $row['nrf2'];
                            $recipes[$rcp_id]->url = $row['url'];
                            $recipes[$rcp_id]->score = $row['score'];
                            $recipes[$rcp_id]->prep_time = $row['prep_time'];
                            $recipes[$rcp_id]->cook_time = $row['cook_time'];
                            $recipes[$rcp_id]->ready_time = $row['ready_time'];
                            $recipes[$rcp_id]->calories = $row['calories'];
                            $recipes[$rcp_id]->servings = $row['servings'];
                            $recipes[$rcp_id]->carb_per = $row['carb_per'];
                            $recipes[$rcp_id]->fat_per = $row['fat_per'];
                            $recipes[$rcp_id]->protein_per = $row['protein_per'];
                            $recipes[$rcp_id]->total_per = $row['total_per'];
                            $recipes[$rcp_id]->cumulative_score_1 = $row['cumulative_score_1'];
                            $recipes[$rcp_id]->cumulative_score_2 = $row['cumulative_score_2'];
                            $recipes[$rcp_id]->reviews = $row['reviews'];
                            $recipes[$rcp_id]->macronutrient_balance = $macronutrient_balance;
                            $recipes[$rcp_id]->ingredients_in_recipe_num = $counter;
                            $recipes[$rcp_id]->carb_score = number_format((float)$carb_score, 2);
                            $recipes[$rcp_id]->protein_score = number_format((float)$protein_score, 2);
                            $recipes[$rcp_id]->fat_score = number_format((float)$fat_score, 2);;
                            $recipes[$rcp_id]->macronutrient_score = number_format((float)$macronutrient_score, 2);
                        }
                    }
                }

                //fetching total_steps number for each recipe
                $result = mysqli_query($connection, "SELECT MAX(step) FROM recipe_instruction WHERE recipe_id = $rcp_id") or die("Could not execute query");

                if($result !== false)
                {
                    if($row = $result->fetch_array(MYSQLI_ASSOC))
                    {
                        $recipes[$rcp_id]->total_steps = $row['MAX(step)'];
                    }
                }

                //fetching ingredients for each recipe
                $result = mysqli_query($connection, "SELECT content as ingredient FROM ingredient_content
                INNER JOIN ingredient_list ON ingredient_content.id = ingredient_list.ingredient_content_id
                WHERE recipe_id = $rcp_id") or die("Could not execute query");
            
                if($result !== false)
                {
                    while($row = $result->fetch_array(MYSQLI_ASSOC))
                    {
                        $recipes[$rcp_id]->ingredient_list[] = $row['ingredient'];
                    }
                    
                    $ingredients_present = array();
                    $ingredients_present = find_ingredients($ingredients, $recipes[$rcp_id]->ingredient_list);
                    
                    foreach($ingredients_present as $ingredient_present)
                    {
                        $recipes[$rcp_id]->ingredients_in_recipe[] = $ingredient_present;
                    }
                }
                
                //fetching instructions for each recipe
                $result = mysqli_query($connection, "SELECT instruction FROM instruction
                INNER JOIN recipe_instruction ON instruction.id = recipe_instruction.instruction_id
                WHERE recipe_id = $rcp_id ORDER BY recipe_instruction.step ASC") or die("Could not execute query");
            
                if($result !== false)
                {
                    $i = 1;
                    while($row = $result->fetch_array(MYSQLI_ASSOC))
                    {
                        $recipes[$rcp_id]->instructions[$i] = $row['instruction'];
                        $i++;
                    }
                }

                //fetching nutrients for each recipe
                $result = mysqli_query($connection, "SELECT nutrient.nutrient, recipe_nutrient.nutrient_value as value, recipe_nutrient.daily_value
                FROM nutrient INNER JOIN recipe_nutrient ON nutrient.id = recipe_nutrient.nutrient_id
                WHERE recipe_id = $rcp_id ORDER BY recipe_nutrient.nutrient_id ASC") or die("Could not execute query");
            
                if($result !== false)
                {
                    while($row = $result->fetch_array(MYSQLI_ASSOC))
                    {
                        $recipes[$rcp_id]->nutrients[] = $row;
                    }
                    if($recipes[$rcp_id]->nutrients === null)
                    {
                        $recipes[$rcp_id]->nutrients = [];
                    }
                }
            }
            $counter -= 1;
        }
        
        //order by number of matching ingredients, score and then nrf 
        foreach ($recipes as $recipe)
        {
            $ingredients_in_recipe_num[] = $recipe->ingredients_in_recipe_num;
            $score[] = $recipe->score;
            $nrf[] = $recipe->nrf;
        }
        array_multisort ($ingredients_in_recipe_num, SORT_DESC, $score, SORT_DESC, $nrf, SORT_DESC, $recipes);

        //clear ids as keys
        $recipes = array_values($recipes);
        mysqli_free_result($result);
        mysqli_close($connection);
        return $recipes;
    }
    catch (Exception $e) 
    {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
        echo "Ingredient does not exist";
    }
}

function handle_request_numbers()
{
    //requested number of recipes
    try
    {
        $requested_number = intval($_GET["number"]);
        //check if requested number is an integer beetwen 0 and $REQUESTED_NUMBER 
        if((is_int($requested_number) && $requested_number >= 0 && $requested_number < $GLOBALS['REQUESTED_NUMBER']) === false)
        {
            $requested_number = $GLOBALS['REQUESTED_NUMBER'];
        }
    }
    catch (TypeError $e)
    {
        $requested_number = $GLOBALS['REQUESTED_NUMBER'];
    }
    //number of recipes from and to
    try
    {
        $from_number = intval($_GET["from"]);
        if((is_int($from_number) && $from_number >= 0) === false)
        {
            $from_number = 0;
        }
    }
    catch (TypeError $e)
    {
        $from_number = 0;
    }
    try
    {
        $to_number = intval($_GET["to"]);
        if((is_int($to_number) && $to_number > 0) === false)
        {
            $to_number = $GLOBALS['REQUESTED_NUMBER'];
        }
    }
    catch (TypeError $e)
    {
        $to_number = $GLOBALS['REQUESTED_NUMBER'];
    }

    if ($from_number > $to_number)
    {
        $from_number = $to_number - 10;
    }

    return array($requested_number, $from_number, $to_number);
}

//--------------------------------------------------------------------------------//
try
{
    $ingredients = explode(",", $_GET["ingredients"]);
}
catch (Exception $e) 
{
    echo 'Caught exception: ', $e->getMessage(), "\n";
    echo "Could not get ingredients\n";
}
try
{
    $category = $_GET["category"];
    if(!$category)
    {
        $category = "All";
    }
}
catch (Exception $e) 
{
    echo 'Caught exception: ', $e->getMessage(), "\n";
    echo "Could not get category\n";
}

list($requested_number, $from_number, $to_number) = handle_request_numbers();

if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
    try
    {
        echo json_encode(get_request_data($ingredients, $category, $requested_number, $from_number, $to_number));
    }
    catch (TypeError $e)
    {
        echo 'Caught exception: ', $e->getMessage(), "\n";
        echo "No data";
    }
}
////FOR TESTING
// if(regular_expression("eggs", "3 egg"))
// {
//     echo "TRUE";
// }
// else
// {
//     echo "FALSE";
// }
?>