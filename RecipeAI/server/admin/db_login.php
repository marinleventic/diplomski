<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
?>