<?php
require_once('/admin/db_login.php');
header("Access-Control-Allow-Origin: *");
header("Content-type:application/json");

class Ingredient
{
    public $id;
    public $value;
}

try
{
    $result = mysqli_query($connection, "SELECT id, ingredient FROM ingredient") or die('Could not execute query');

    // $number_of_ingredients = mysqli_num_rows($result);

    // echo "There are " . " " . $number_of_ingredients . " " . " ingredients in the database.";
    // echo "<br><br>";
    
    $ingredient_list = [];
    $i = 0;

    if(mysqli_num_rows($result))
    {
        while($row = $result->fetch_array(MYSQLI_ASSOC))
        {
            $ingredient_list[$i] = new Ingredient();
            $ingredient_list[$i]->id = $row['id'];
            $ingredient_list[$i]->value = mb_strtolower($row['ingredient'], 'UTF-8');
            $i++;
        }
    }
}
catch (Exception $e) 
{
    echo 'Caught exception: ', $e->getMessage(), "\n";
    echo "Problems while fetching ingredients";
}

mysqli_free_result($result);

if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
    try
    {
        echo json_encode($ingredient_list);
    }
    catch (TypeError $e)
    {
        echo 'Caught exception: ', $e->getMessage(), "\n";
        echo "Not a JSON serializable object";
    }
}

mysqli_close($connection);
?>