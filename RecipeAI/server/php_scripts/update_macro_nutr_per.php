<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

$macro_nutrient_ids = array(1, 6, 8);
$number_of_updated_values = 0;
$number_of_invalid_values = 0;
$number_of_zero_values = 0;
$number_of_recipes_with_zero_calories = 0;

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Count number of rows
$result = (mysqli_query($connection, "SELECT id FROM recipe"));
while ($row = $result->fetch_array())
{
    $recipe_ids[] = $row['id'];
}
$number_of_recipes = count($recipe_ids);

echo "There are " . " " . $number_of_recipes . " " . " recipes in the database.";
echo "<br><br>";

echo "Updating 'carb_per', 'fat_per' and 'protein_per' in 'recipe' table..." . "<br>";

$time_pre = microtime(true);

//FOR TESTING
//foreach (array_slice($recipe_ids, 0, 10) as $recipe_id)
foreach ($recipe_ids as $recipe_id)
{
    $calories = 0;
    $carb_per = 0;
    $fat_per = 0;
    $protein_per = 0;
    $total_per = 0;

    foreach ($macro_nutrient_ids as $macro_nutrient_id)
	{
        $nutrient_value = 0;
        
        if ($result = mysqli_query($connection, "SELECT recipe_nutrient.nutrient_value, recipe.carb_per, recipe.fat_per, recipe.protein_per, recipe.calories
            FROM recipe_nutrient INNER JOIN recipe ON recipe_nutrient.recipe_id = recipe.id WHERE recipe_nutrient.recipe_id='$recipe_id'
            AND recipe_nutrient.nutrient_id='$macro_nutrient_id';"))
        {
            //if calories already selected from query don't do it again
            if($calories == 0)
            {
                $calories = $row['calories'];
            }

            if ($row = $result->fetch_array())
            {
                //if nutrient_value contains '<' set it's value to 0 (because it only appears if it's <1mg)
                if (strpos($row['nutrient_value'], '<') !== false)
                {
                    $nutrient_value = 0;
                }
                //if nutrient_value is in micrograms calculate it in grams
                else if (strpos($row['nutrient_value'], 'mcg') !== false)
                {
                    if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
                    $nutrient_value = floatval($pieces[0])/1000000;
                }
                //if nutrient_value is in miligrams calculate it in grams
                else if (strpos($row['nutrient_value'], 'mg') !== false)
                {
                    if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
                    $nutrient_value = floatval($pieces[0])/1000;
                }
                //if nutrient_value is in grams leave it
                else if (strpos($row['nutrient_value'], 'g') !== false)
                {
                    if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
                    $nutrient_value = floatval($pieces[0]);
                }
                //if nutrient_value is in IU
                else if (strpos($row['nutrient_value'], 'IU') !== false)
                {
                    if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
                    $nutrient_value = 0;
                }
                //else it only contains number
                else
                {
                    $nutrient_value = floatval($row['nutrient_value']);
                }

                if ($row['calories'] != 0)
                {
                    //Total Fat
                    if ($macro_nutrient_id == 1)
                    {
                        $fat_per = round(($nutrient_value * 9 / $row['calories'] ) * 100);
                    }
                    //Total Carbohydrates
                    else if ($macro_nutrient_id == 6)
                    {
                        $carb_per = round(($nutrient_value * 4 / $row['calories']) * 100);
                    }
                    //Protein
                    else if ($macro_nutrient_id == 8)
                    {
                        $protein_per = round(($nutrient_value * 4 / $row['calories'] ) * 100);
                    }
                }
            }
            // Free result set
            mysqli_free_result($result);
        }
    }
    
    //if total percentage is not close to 100 
    if ((($carb_per + $protein_per + $fat_per) > 0) && (($carb_per + $protein_per + $fat_per) < 99) || (($carb_per + $protein_per + $fat_per) > 101))
    {
        $total_per = $carb_per + $protein_per + $fat_per;

        $carb_per = round(($carb_per / $total_per) * 100);
        $fat_per = round(($fat_per / $total_per) * 100);
        $protein_per = round(($protein_per / $total_per) * 100);
    }

    //if total percentage is still not 100
    if (($carb_per + $protein_per + $fat_per) == 0)
    {
        $number_of_zero_values++;
    }
    else if (($carb_per + $protein_per + $fat_per) != 100)
    {
        $carb_per = $carb_per + 100 - ($carb_per + $protein_per + $fat_per);
    }

    $total_per = $carb_per + $protein_per + $fat_per;

    if ($total_per != 100)
    {
        // echo "<i><b> - " . $recipe_id . " | total_per: " . $total_per . " - </b></i><br>";
        $number_of_invalid_values++;
    }

    // echo $recipe_id . " | carb_per: " . $carb_per . " | fat_per: " . $fat_per . " | protein_per: " . $protein_per . " | calories: " . $row['calories'] . " | total_per: " . $total_per;
    // echo "<br>";

    if ($calories != 0)
    {
        if (mysqli_query($connection, "UPDATE recipe SET carb_per=$carb_per, fat_per=$fat_per, protein_per=$protein_per, total_per=$total_per WHERE id=$recipe_id"))
        {
            $number_of_updated_values++;
        } 
        else 
        {
            echo "Error: " . "<br>" . mysqli_error($connection);
        }
    }
    else
    {
        $number_of_recipes_with_zero_calories++;
    }
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "<br>Updated " . $number_of_updated_values . "/" . $number_of_recipes . " recipes.<br>";
echo "<br>" . $number_of_invalid_values . "/" . $number_of_recipes . " recipes has total percentage value < 100 or > 100.<br>";
echo "<br>" . $number_of_zero_values . "/" . $number_of_recipes . " recipes has no macronutrient percentage values.<br>";
echo "<br>" . $number_of_recipes_with_zero_calories . "/" . $number_of_recipes . " recipes has 0 calories.<br>" . "It took " . $exec_time . " seconds to update.";

mysqli_close($connection);
?>