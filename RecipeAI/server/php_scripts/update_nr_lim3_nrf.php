<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

$nr_nutrient_ids = array(5, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18);
$lim3_nutrient_ids = array(2, 4, 9);
$number_of_updated_nr_values = 0;
$number_of_updated_lim3_values = 0;
$number_of_updated_nrf_values = 0;
//Contains all nutrient ids needed for calculating NR and LIM3
$nutrient_ids = array_merge($nr_nutrient_ids, $lim3_nutrient_ids);

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Count number of recipes
$result = (mysqli_query($connection, "SELECT id FROM recipe"));
while ($row = $result->fetch_array())
{
	$recipe_ids[] = $row['id'];
}
$number_of_recipes = count($recipe_ids);

echo "There are " . " " . $number_of_recipes . " " . " recipes in the database.";
echo "<br><br>";

//----------------------------------------------------------NR and LIM3 values----------------------------------------------------------//
echo "Updating 'nr' and 'lim3' values in 'recipe' table...";
echo "<br>";

$time_pre = microtime(true);

////FOR TESTING
// foreach (array_slice($recipe_ids, 0, 10) as $recipe_id)
foreach ($recipe_ids as $recipe_id)
{
    $nr_value = 0;
    $lim3_value = 0;
    $nrf_value = 0;
	$current_nutrient_value = 0;
	$current_nutrient_daily_value = 0;

	foreach ($nutrient_ids as $nutrient_id)
	{
		if ($result = mysqli_query($connection, "SELECT recipe_nutrient.nutrient_value, nutrient.nutrient, nutrient.recommended_value
        FROM recipe_nutrient INNER JOIN nutrient ON recipe_nutrient.nutrient_id = nutrient.id WHERE recipe_nutrient.recipe_id='$recipe_id'
        AND recipe_nutrient.nutrient_id='$nutrient_id';"))
		{
			if ($row = $result->fetch_array())
			{
				//if nutrient_value contains '<' set it's value to 0 (because it only appears if it's <1mg)
				if (strpos($row['nutrient_value'], '<') !== false)
				{
					$current_nutrient_value = 0;
				}
				//if nutrient_value is in micrograms calculate it in grams
				else if (strpos($row['nutrient_value'], 'mcg') !== false)
				{
					if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
					$current_nutrient_value = floatval($pieces[0])/1000000;
				}
				//if nutrient_value is in miligrams calculate it in grams
				else if (strpos($row['nutrient_value'], 'mg') !== false)
				{
					if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
					$current_nutrient_value = floatval($pieces[0])/1000;
				}
				//if nutrient_value is in grams leave it
				else if (strpos($row['nutrient_value'], 'g') !== false)
				{
					if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
					$current_nutrient_value = floatval($pieces[0]);
				}
				//if nutrient value is in IU leave it in IU
				else if (strpos($row['nutrient_value'], 'IU') !== false)
				{
					$current_nutrient_value = $row['nutrient_value'];
				}
				//else it only contains number
				else
				{
					$current_nutrient_value = floatval($row['nutrient_value']);
				}

                if (in_array($nutrient_id, $nr_nutrient_ids))
                {
					//if Vitamin A value is in IU divide by recommended value in IU (2666IU)
					if(strpos($current_nutrient_value, 'IU') !== false)
					{
						if(($pieces = preg_split('/(?=[a-zA-Z])/',$current_nutrient_value)) !== false)
						{
							$current_nutrient_value = floatval($pieces[0]);
							$current_nutrient_daily_value = $current_nutrient_value / 2666;
						}
					}
					else
					{
						$current_nutrient_daily_value = $current_nutrient_value / $row['recommended_value'];
					}
					//if daily value is bigger than 100% set it to 100%

					if($current_nutrient_daily_value > 1)
					{
						$current_nutrient_daily_value = 1;
					}

                    $nr_value = $nr_value + $current_nutrient_daily_value;
                    // echo $row['id'] . " | " . $row['nutrient'] . " | nutrient value: " . $current_nutrient_value . " | recommended value: " . $row['recommended_value'];
                    // echo "<br>";
                }
                else if (in_array($nutrient_id, $lim3_nutrient_ids))
                {
					$current_nutrient_daily_value = $current_nutrient_value / $row['recommended_value'];

					//if daily value is bigger than 100% set it to 100%
					if($current_nutrient_daily_value > 1)
					{
						$current_nutrient_daily_value = 1;
					}
                    $lim3_value = $lim3_value + $current_nutrient_daily_value;
                    // echo $row['id'] . " | " . $row['nutrient'] . " | nutrient value: " . $current_nutrient_value . " | maximum recommended value: " . $row['recommended_value'];
                    // echo "<br>";
				}
			}
			// Free result set
			mysqli_free_result($result);
		}	
	}
	//update recipe's NR value
	if (mysqli_query($connection, "UPDATE recipe SET nr=$nr_value WHERE id=$recipe_id"))
	{
		$number_of_updated_nr_values++;
	}
	else
	{
		echo "Error: " . "<br>" . mysqli_error($connection);
	}
	// echo $recipe_id . " | " . "NR value for this recipe is" . " " . $nr_value;
    // echo "<br>";

    //Update recipe's LIM3 value
	if (mysqli_query($connection, "UPDATE recipe SET lim3=$lim3_value WHERE id=$recipe_id"))
	{
		$number_of_updated_lim3_values++;
	}
	else
	{
		echo "Error: " . "<br>" . mysqli_error($connection);
	}
	// echo $recipe_id . " | " . "LIM3 value for this recipe is" . " " . $lim3_value;
	// echo "<br>";
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "Updated " . $number_of_updated_nr_values . "/" . $number_of_recipes . " 'nr' values and " . $number_of_updated_lim3_values . "/" . $number_of_recipes . " 'lim3' values. <br>" . "It took " . $exec_time . " seconds to update.<br><br>";

//----------------------------------------------------------NRF values----------------------------------------------------------//
echo "Updating 'nrf' values in 'recipe' table...";
echo "<br>";

$number_of_updated_nrf_values = 0;

$time_pre = microtime(true);
foreach ($recipe_ids as $recipe_id)
{
	$nrf_value = 0;

	if ($result = mysqli_query($connection, "SELECT nr, lim3 FROM recipe WHERE recipe.id='$recipe_id';"))
	{
		if ($row = $result->fetch_array())
		{
			$nrf_value = $row['nr'] - $row['lim3'];
		}
	}
	
	if (mysqli_query($connection, "UPDATE recipe SET nrf=$nrf_value WHERE id=$recipe_id"))
	{
		$number_of_updated_nrf_values++;
	}
	else
	{
		echo "Error: " . "<br>" . mysqli_error($connection);
	}
	// echo $recipe_id . " | " . "NRF value for this recipe is" . " " . $nrf_value;
	// echo "<br>";	

	// Free result set
	mysqli_free_result($result);
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "Updated " . $number_of_updated_nrf_values . "/" . $number_of_recipes . " 'nrf' values.<br>" . "It took " . $exec_time . " seconds to update.";

mysqli_close($connection);
?>