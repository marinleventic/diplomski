<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

// Select query returns a resultset
if ($result = mysqli_query($connection, "SELECT DISTINCT nutrient_value FROM recipe_nutrient")) {
    printf("Select returned %d rows.", mysqli_num_rows($result));
	echo "<br />";
	
	while($row = $result->fetch_array())
	{
		echo $row['nutrient_value']/* . " | " . $row['nutrient']*/;
		echo "<br>";
	}
	
    // Free result set
    mysqli_free_result($result);
}

mysqli_close($connection);
?>

