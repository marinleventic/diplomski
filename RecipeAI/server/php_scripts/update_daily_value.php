<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

$nutrient_value = 0;
$daily_value = 0;
$number_of_updated_values = 0;

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Count number of rows
$result = (mysqli_query($connection, "SELECT id FROM recipe_nutrient"));
while ($row = $result->fetch_array())
{
    $recipe_nutrient_ids[] = $row['id'];
}
$number_of_rows = count($recipe_nutrient_ids);

echo "There are " . " " . $number_of_rows . " " . " rows in the database.";
echo "<br>";

echo "Updating 'daily_value' in 'recipe_nutrient' table..." . "<br>" . "<br>";

$time_pre = microtime(true);

//FOR TESTING
//foreach (array_slice($recipe_nutrient_ids, 0, 10) as $recipe_nutrient_id)
foreach ($recipe_nutrient_ids as $recipe_nutrient_id)
{
    if ($result = mysqli_query($connection, "SELECT recipe_nutrient.nutrient_value, recipe_nutrient.daily_value, nutrient.id, nutrient.recommended_value
        FROM recipe_nutrient INNER JOIN nutrient ON recipe_nutrient.nutrient_id = nutrient.id WHERE recipe_nutrient.id='$recipe_nutrient_id';"))
    {
        if ($row = $result->fetch_array())
        {
            //if nutrient_value contains '<' set it's value to 0 (because it only appears if it's <1mg)
            if (strpos($row['nutrient_value'], '<') !== false)
            {
                $nutrient_value = 0;
            }
            //if nutrient_value is in micrograms calculate it in grams
            else if (strpos($row['nutrient_value'], 'mcg') !== false)
            {
                if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
                $nutrient_value = floatval($pieces[0])/1000000;
            }
            //if nutrient_value is in miligrams calculate it in grams
            else if (strpos($row['nutrient_value'], 'mg') !== false)
            {
                if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
                $nutrient_value = floatval($pieces[0])/1000;
            }
            //if nutrient_value is in grams leave it
            else if (strpos($row['nutrient_value'], 'g') !== false)
            {
                if(($pieces = preg_split('/(?=[a-zA-Z])/',$row['nutrient_value'])) !== false)
                $nutrient_value = floatval($pieces[0]);
            }
            //if nutrient value is in IU leave it in IU
            else if (strpos($row['nutrient_value'], 'IU') !== false)
            {
                $nutrient_value = $row['nutrient_value'];
            }
            //else it only contains number
            else
            {
                $nutrient_value = floatval($row['nutrient_value']);
            }

            //don't update values where nutrient recommended value is null
            if($row['recommended_value'] !== null)
            {
                //if Vitamin A value is in IU divide by recommended value in IU (2666IU)
                if (strpos($nutrient_value, 'IU') !== false)
                {
                    if(($pieces = preg_split('/(?=[a-zA-Z])/',$nutrient_value)) !== false)
                    {
                        $nutrient_value = floatval($pieces[0]);
                        $daily_value = ($nutrient_value / 2666) * 100;
                    }
                }
                else
                {
                    $daily_value = ($nutrient_value / $row['recommended_value']) * 100;
                }
                
                if (mysqli_query($connection, "UPDATE recipe_nutrient SET daily_value=$daily_value WHERE id=$recipe_nutrient_id"))
                {
                    $number_of_updated_values++;
                } 
                else 
                {
                    echo "Error: " . "<br>" . mysqli_error($connection);
                }
            }
        }
        // Free result set
        mysqli_free_result($result);
    }
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "Updated " . $number_of_updated_values . "/" . $number_of_rows . " rows.<br>"  . "It took " . $exec_time . " seconds to update.";

mysqli_close($connection);
?>