<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

$number_of_updated_values = 0;
$reviews_median = 0;

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Count number of recipes
$result = (mysqli_query($connection, "SELECT id FROM recipe"));
while ($row = $result->fetch_array())
{
    $recipe_ids[] = $row['id'];
}
$number_of_recipes = count($recipe_ids);

echo "There are " . " " . $number_of_recipes . " " . " recipes in the database.";
echo "<br><br>";

//Calculate reviews median
$result = (mysqli_query($connection, "SET @rowindex := -1;"));
$result = (mysqli_query($connection, "SELECT
   AVG(reviews)
FROM
   (SELECT @rowindex:=@rowindex + 1 AS rowindex,
           recipe.reviews AS reviews
    FROM recipe
    ORDER BY recipe.reviews) AS r
WHERE
r.rowindex IN (FLOOR(@rowindex / 2) , CEIL(@rowindex / 2));"));

if ($row = $result->fetch_array())
{
    $reviews_median = $row['AVG(reviews)'];
}

echo "Median for number of reviews is: " . $reviews_median;
echo "<br><br>";
//----------------------------------------------------------Score----------------------------------------------------------//
echo "Updating 'adjusted_rating' and 'score' values in 'recipe' table...";
echo "<br><br>";

$time_pre = microtime(true);

//FOR TESTING
//foreach (array_slice($recipe_ids, 0, 10) as $recipe_id)
foreach ($recipe_ids as $recipe_id)
{
    $adjusted_rating = 0;
    $score = 0;

    if ($result = mysqli_query($connection, "SELECT rating, reviews FROM recipe WHERE id='$recipe_id';"))
    {
        if ($row = $result->fetch_array())
        {
            if ($row['reviews'] >= $reviews_median)
            { 
                $adjusted_rating = $row['rating'];
            }            
            else 
            {
                $adjusted_rating = ( $row['rating'] * $row['reviews']) / $reviews_median;
            }
            $score = $adjusted_rating / 5;
            // echo $recipe_id;
            // echo " | reviews: " . $row['reviews'] . " | rating: " . $row['rating'];
            // echo " | adjusted_rating: " . $adjusted_rating . " | score: " . $score;
            // echo "<br>";
        }
        // Free result set
        mysqli_free_result($result);
    }

//Update recipe's score values
    if (mysqli_query($connection, "UPDATE recipe SET adjusted_rating=$adjusted_rating, score=$score WHERE id=$recipe_id"))
    {
        $number_of_updated_values++;
    }
    else
    {
        echo "Error: " . "<br>" . mysqli_error($connection);
    }
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "Updated " . $number_of_updated_values . "/" . $number_of_recipes . " rows. <br>" . "It took " . $exec_time . " seconds to update.";

mysqli_close($connection);
?>