<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

$number_of_updated_values = 0;
$number_of_recipes_with_zero_calories = 0;

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Count number of recipes
$result = (mysqli_query($connection, "SELECT id FROM recipe"));
while ($row = $result->fetch_array())
{
    $recipe_ids[] = $row['id'];
}
$number_of_recipes = count($recipe_ids);

echo "There are " . " " . $number_of_recipes . " " . " recipes in the database.";
echo "<br><br>";

//----------------------------------------------------------NR2, LIM32 and NRF2 values (values for 100kcal)----------------------------------------------------------//
echo "Updating 'nr2', 'lim32' and 'nrf2' values in 'recipe' table (values for 100kcal)...";
echo "<br><br>";

$time_pre = microtime(true);

//FOR TESTING
//foreach (array_slice($recipe_ids, 0, 10) as $recipe_id)
foreach ($recipe_ids as $recipe_id)
{
    $nr2 = 0;
    $lim32 = 0;
    $nrf2 = 0;
    $calories_per_serving = 0;

    if ($result = mysqli_query($connection, "SELECT nr, lim3, nrf, calories, servings FROM recipe WHERE id='$recipe_id';"))
    {
        if ($row = $result->fetch_array())
        {
            if ($row['servings'] != 0)
            {
                $calories_per_serving = $row['calories'] / ($row['servings']);
                // echo "Calories per serving: " . $calories_per_serving . "<br>";
                
                if ($calories_per_serving != 0)
                {
                    $nr2 = ($row['nr'] / $calories_per_serving) * 100 ;
                    $lim32 = ($row['lim3'] / $calories_per_serving) * 100 ;
                    $nrf2 = ($row['nrf'] / $calories_per_serving) * 100 ;
                }
                else
                {
                    $number_of_recipes_with_zero_calories++;
                    //echo "<i><b> -- ID " . $recipe_id . "-- Recipe has 0 calories per serving so NR2, LIM32 and NRF2 values couldn't be calculated.</b></i> <br>";
                }
            }
            else
            {
                echo "<i><b> -- ID " . $recipe_id . "-- Recipe has no defined servings amount so NR2, LIM32 and NRF2 values couldn't be calculated.</b></i> <br>";
            }
        }
        // Free result set
        mysqli_free_result($result);
    }	
    //update recipe's NR2, LIM32 and NRF2 values
    if ($calories_per_serving != 0)
    {
        if (mysqli_query($connection, "UPDATE recipe SET nr2=$nr2, lim32=$lim32, nrf2=$nrf2 WHERE id=$recipe_id"))
        {
            $number_of_updated_values++;
            // echo $recipe_id . " | NR2 value: " . $nr2 . " | LIM32 value: " . $lim32 . " | NRF2 value: " . $nrf2;
            // echo "<br>";
        }
        else
        {
            echo "Error: " . "<br>" . mysqli_error($connection);
        }
    }
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "Updated " . $number_of_updated_values . "/" . $number_of_recipes . " rows. <br>";
echo "<br>" . $number_of_recipes_with_zero_calories . "/" . $number_of_recipes . " recipes has 0 calories.<br>" . "It took " . $exec_time . " seconds to update.";

mysqli_close($connection);
?>