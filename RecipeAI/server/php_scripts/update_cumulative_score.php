<?php
$servername = "localhost";
$username = "root";
$password = "password";
$db_name = "recipes_db";

$number_of_updated_values = 0;
$max_nrf2 = 0;

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db_name);

// Check connection
if (mysqli_connect_errno())
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Count number of recipes
$result = (mysqli_query($connection, "SELECT id FROM recipe"));
while ($row = $result->fetch_array())
{
    $recipe_ids[] = $row['id'];
}
$number_of_recipes = count($recipe_ids);

echo "There are " . " " . $number_of_recipes . " " . " recipes in the database.";
echo "<br><br>";

$result = mysqli_query($connection, "SELECT MAX(nrf2) FROM recipe;");

if ($row = $result->fetch_array())
{
    $max_nrf2 = $row['MAX(nrf2)'];

    // echo "Max NRF2 value is " . " " . $max_nrf2 . ".";
    // echo "<br><br>";
}

//----------------------------------------------------------Cumulative score 1 and Cumulative score 2----------------------------------------------------------//
echo "Updating 'cumulative_score_1' and 'cumulative_score_2' values in 'recipe' table...";
echo "<br><br>";

$time_pre = microtime(true);

//FOR TESTING
//foreach (array_slice($recipe_ids, 0, 10) as $recipe_id)
foreach ($recipe_ids as $recipe_id)
{
    $cumulative_score_1 = 0;
    $macronutrient_score = 0;
    $carb_score = 0;
    $fat_score = 0;
    $protein_score = 0;
    $cumulative_score_2 = 0;
    $carb_prob = 0;
    $fat_prob = 0;
    $protein_prob = 0;

    if ($result = mysqli_query($connection, "SELECT score, nrf2, carb_per, fat_per, protein_per FROM recipe WHERE id='$recipe_id';"))
    {
        if ($row = $result->fetch_array())
        {
            //Calculate Cumulative score 1
            if ($row['carb_per'] < 45) { $carb_score = $row['carb_per'] / 45; }
            else if  (($row['carb_per'] >= 45) && ($row['carb_per'] <= 65)) { $carb_score = 1; }
            else if  ($row['carb_per'] >= 65) { $carb_score = -(65 / $row['carb_per']); }

            if ($row['fat_per'] < 25) { $fat_score = $row['fat_per'] / 25; }
            else if  (($row['fat_per'] >= 25) && ($row['fat_per'] <= 35)) { $fat_score = 1; }
            else if  ($row['fat_per'] >= 35) { $fat_score = -(35 / $row['fat_per']); }

            if ($row['protein_per'] < 10) { $protein_score = $row['protein_per'] / 10; }
            else if  (($row['protein_per'] >= 10) && ($row['protein_per'] <= 35)) { $protein_score = 1; }
            else if  ($row['protein_per'] >= 35) { $protein_score = -(35 / $row['protein_per']); }

            $macronutrient_score = ($carb_score + $fat_score + $protein_score) / 3;
            $cumulative_score_1 = (($row['nrf2'] / $max_nrf2) + $macronutrient_score + $row['score']) / 3;

            //Calculate Cumulative score 2
            if ($row['carb_per'] < 45) { $carb_prob = $row['carb_per'] / 45; }
            else if  (($row['carb_per'] >= 45) && ($row['carb_per'] <= 65)) { $carb_prob = 1; }
            else if  ($row['carb_per'] >= 65) { $carb_prob = 0; }

            if ($row['fat_per'] < 25) { $fat_prob = $row['fat_per'] / 25; }
            else if  (($row['fat_per'] >= 25) && ($row['fat_per'] <= 35)) { $fat_prob = 1; }
            else if  ($row['fat_per'] >= 35) { $fat_prob = 0; }

            if ($row['protein_per'] < 10) { $protein_prob = $row['protein_per'] / 10; }
            else if  (($row['protein_per'] >= 10) && ($row['protein_per'] <= 35)) { $protein_prob = 1; }
            else if  ($row['protein_per'] >= 35) { $protein_prob = 0; }

            if ($row['nrf2'] > 0)
            {
                $cumulative_score_2 = ($row['nrf2'] / $max_nrf2) * $carb_prob * $fat_prob * $protein_prob * $row['score'];
            }
            else
            {
                $cumulative_score_2 = 0 * $carb_prob * $fat_prob * $protein_prob * $row['score'];
            }

            // echo $recipe_id;
            // echo " | carb_score: " . $carb_score . " | fat_score: " . $fat_score . " | protein_score: " . $protein_score . " | macronutrient_score: " . $macronutrient_score;
            // echo " | carb_prob: " . $carb_prob . " | fat_prob: " . $fat_prob . " | protein_prob: " . $protein_prob;
            // echo " | Cumulative score1: " . $cumulative_score_1 . " | Cumulative score 2: " . $cumulative_score_2;
            // echo "<br>";
        }
        // Free result set
        mysqli_free_result($result);
    }

//Update recipe's Cumulative scores
    if (mysqli_query($connection, "UPDATE recipe SET cumulative_score_1=$cumulative_score_1, cumulative_score_2=$cumulative_score_2 WHERE id=$recipe_id"))
    {
        $number_of_updated_values++;
    }
    else
    {
        echo "Error: " . "<br>" . mysqli_error($connection);
    }
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "Updated " . $number_of_updated_values . "/" . $number_of_recipes . " rows. <br>" . "It took " . $exec_time . " seconds to update.";

mysqli_close($connection);
?>