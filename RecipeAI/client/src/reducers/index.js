import { combineReducers } from 'redux';
import ingredientsReducer from 'reducers/ingredients';
import recipesReducer from 'reducers/recipes';

export default combineReducers({
  ingredients: ingredientsReducer,
  recipes: recipesReducer
});