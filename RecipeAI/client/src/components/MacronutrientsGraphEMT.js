import React, { Component } from 'react';
import EMT from '../images/thumbnail_EMT.png';

class MacronutrientsGraphEMT extends Component {

    constructor(props) {
        super(props);

        this.state = {
            width: 1280,
            height: 1038,
            carb_per : 0,
            protein_per : 0, 
            fat_per : 0,
            isMobile: false
        };

        this.onResize = this.onResize.bind(this);
    }

    componentWillMount() {
        const carb_per = this.props.carb_per;
        const protein_per = this.props.protein_per;
        const fat_per = this.props.fat_per;
        const width = this.props.width;
        const height = this.props.height;

        if(width)
        {
            this.setState({ width : width });
        }

        if(height)
        {
            this.setState({ height : height });
        }

        if(carb_per)
        {
            this.setState({ carb_per : carb_per });
        }

        if(protein_per)
        {
            this.setState({ protein_per : protein_per });
        }

        if(fat_per)
        {
            this.setState({ fat_per : fat_per });
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.onResize);

        const canvas = this.refs.canvas;
        const ctx = canvas.getContext("2d");
        const img = this.refs.image;
        const lineWidth = canvas.width / 300;
        const circleDiameter = canvas.width / 100;
        const lineColor = "#fa41fe";

        img.onload = () => {

            ctx.drawImage(img, 0, 0, img.width, img.height,         // source rectangle
                            0, 0, canvas.width, canvas.height);     // destination rectangle

            //Fat
            let startX = canvas.width / 2 - canvas.width / 200;
            let startY = canvas.height / 19;
            let endX = canvas.width / 21.49;
            let endY = canvas.height - canvas.height / 7.5;

            let graphAngle = 2.168;
            let lineAngle = 0.987;
            let fullValue = Math.sqrt(Math.pow((endX - startX), 2) + Math.pow((endY - startY), 2));

            const fatLine = this.returnMacronutrientLine(startX, startY, graphAngle, lineAngle, fullValue, this.state.fat_per);

            //Protein
            startX = canvas.width / 21.49;
            startY = canvas.height - canvas.height / 7.5;
            endX = canvas.width - canvas.width / 16.3;
            endY = canvas.height - canvas.height / 7.5;

            graphAngle = 0;
            lineAngle = 5.308;
            fullValue = Math.abs(endX - startX);

            const proteinLine = this.returnMacronutrientLine(startX, startY, graphAngle, lineAngle, fullValue, this.state.protein_per);

            //Carbohydrates
            startX = canvas.width - canvas.width / 16.3;
            startY = canvas.height - canvas.height / 7.5;
            endX = canvas.width / 2 - canvas.width / 200;
            endY = canvas.height / 19;

            graphAngle = 4.121;
            lineAngle = Math.PI;
            fullValue = Math.sqrt(Math.pow((endX - startX), 2) + Math.pow((endY - startY), 2));

            const carbLine = this.returnMacronutrientLine(startX, startY, graphAngle, lineAngle, fullValue, this.state.carb_per);


            const intersection = this.checkLineIntersection(fatLine.lineStartX, fatLine.lineStartY, fatLine.lineEndX, fatLine.lineEndY,
                                        proteinLine.lineStartX ,proteinLine.lineStartY, proteinLine.lineEndX, proteinLine.lineEndY);

            //if all macronutrient percentages are 0 don't draw anything
            if(parseInt(this.state.fat_per) === 0 && parseInt(this.state.protein_per) === 0 && parseInt(this.state.carb_per) === 0)
            {
                return;
            }
            //if any macronutrient percentage is 100 only draw circle (without dotted lines)
            else if (parseInt(this.state.fat_per) === 100)
            {
                this.drawCircle(ctx, fatLine.fullValueX, fatLine.fullValueY, circleDiameter, lineColor);
            }
            else if (parseInt(this.state.protein_per) === 100)
            {
                this.drawCircle(ctx, proteinLine.fullValueX, proteinLine.fullValueY, circleDiameter, lineColor);
            }
            else if (parseInt(this.state.carb_per) === 100)
            {
                this.drawCircle(ctx, carbLine.fullValueX, carbLine.fullValueY, circleDiameter, lineColor);
            }
            else if (intersection.x && intersection.y)
            {
                //if any macronutrient percentage is 0 don't draw dotted line
                if(parseInt(this.state.fat_per) !== 0)
                {
                    this.drawLine(ctx, fatLine.lineStartX, fatLine.lineStartY, intersection.x, intersection.y, lineWidth, lineColor);
                }
                if(parseInt(this.state.protein_per) !== 0)
                {
                    this.drawLine(ctx, proteinLine.lineStartX, proteinLine.lineStartY, intersection.x, intersection.y, lineWidth, lineColor);
                }
                if(parseInt(this.state.carb_per) !== 0)
                {
                    this.drawLine(ctx, carbLine.lineStartX, carbLine.lineStartY, intersection.x, intersection.y, lineWidth, lineColor);
                }
                //draw circle in intersection of lines 
                this.drawCircle(ctx, intersection.x, intersection.y, circleDiameter, lineColor);
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    }

    returnMacronutrientLine(startX, startY, graphAngle, lineAngle, fullValue, macronutrientPercetange) {
        let lineStartX = startX;
        let lineStartY = startY;

        if(macronutrientPercetange <= 0)
        {
            lineStartX = startX;
            lineStartY = startY;
        }
        else if(macronutrientPercetange > 0 && macronutrientPercetange <= 100)
        {
            lineStartX = startX + (macronutrientPercetange / 100 * fullValue * Math.cos(graphAngle));
            lineStartY = startY + (macronutrientPercetange / 100 * fullValue * Math.sin(graphAngle));
        }
        else if(macronutrientPercetange > 100)
        {
            lineStartX = startX + fullValue * Math.cos(graphAngle);
            lineStartY = startY + fullValue * Math.sin(graphAngle);
        }
        
        let lineEndX = lineStartX + fullValue * Math.cos(lineAngle);
        let lineEndY = lineStartY + fullValue * Math.sin(lineAngle);

        let fullValueX = startX + fullValue * Math.cos(graphAngle);
        let fullValueY = startY + fullValue * Math.sin(graphAngle);

        return {lineStartX, lineStartY, lineEndX, lineEndY, fullValueX, fullValueY};
    }

    checkLineIntersection(line1StartX, line1StartY, line1EndX, line1EndY, line2StartX, line2StartY, line2EndX, line2EndY) {
        // if the lines intersect, the result contains the x and y of the intersection (treating the lines as infinite)
        var denominator, a, b, numerator1, numerator2, result = {
            x: null,
            y: null
        };

        denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX)) - ((line2EndX - line2StartX) * (line1EndY - line1StartY));
        if (denominator === 0) {
            return result;
        }

        a = line1StartY - line2StartY;
        b = line1StartX - line2StartX;
        numerator1 = ((line2EndX - line2StartX) * a) - ((line2EndY - line2StartY) * b);
        numerator2 = ((line1EndX - line1StartX) * a) - ((line1EndY - line1StartY) * b);
        a = numerator1 / denominator;
        b = numerator2 / denominator;
    
        // if we cast these lines infinitely in both directions, they intersect here:
        result.x = line1StartX + (a * (line1EndX - line1StartX));
        result.y = line1StartY + (a * (line1EndY - line1StartY));
        
        /*
        // it is worth noting that this should be the same as:
        x = line2StartX + (b * (line2EndX - line2StartX));
        y = line2StartX + (b * (line2EndY - line2StartY));
        */

        return result;
    }

    drawLine(ctx, lineStartX, lineStartY, lineEndX, lineEndY, lineWidth, lineColor) {
        let dashLength = lineWidth * 3;

        ctx.setLineDash([dashLength, dashLength]);
        ctx.beginPath();
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = lineColor;
        ctx.moveTo(lineStartX, lineStartY);
        ctx.lineTo(lineEndX, lineEndY);
        ctx.stroke();
    }

    drawCircle(ctx, x, y, circleDiameter, lineColor) {
        ctx.beginPath();
        ctx.fillStyle = lineColor;
        ctx.arc(x, y, circleDiameter, 0, 2 * Math.PI);
        ctx.fill();
    }

    onResize()
    {
        if(window.innerWidth <= 768)
        {
        this.setState({isMobile: true});
        }
        else
        {
        this.setState({isMobile: false});
        }
    }

    render() {
        const width = this.state.width;
        const height = this.state.height;
        const mobileStyle = {'width' : '100%', 'height' : '100%', 'minWidth' : '250px', 'padding' : '1em'};
        const desktopStyle = {'width' : '100%', 'height' : '100%', 'minWidth' : '250px', 'padding' : '1em', 'paddingTop' : '21%'};
        return(
            <div>
                <canvas className="macro-EMT-canvas" ref="canvas" width={width} height={height} resize="true" style={this.state.isMobile ? mobileStyle : desktopStyle}/>
                <img ref="image" alt="EMT" src={EMT} className="hidden" />
            </div>
        )
    }
}
export default MacronutrientsGraphEMT